# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-27 02:17+0000\n"
"PO-Revision-Date: 2022-10-01 17:39+0200\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.04.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Nguyễn Hùng Phú"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "phu.nguyen@kdemail.net"

#: main.cpp:76 view.cpp:47
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:76
#, kde-format
msgid "Run Command interface"
msgstr "Giao diện chạy lệnh"

#: main.cpp:82
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Dùng nội dụng bảng nháp làm truy vấn cho KRunner"

#: main.cpp:83
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "Chạy KRunner ở nền, không hiển thị."

#: main.cpp:84
#, kde-format
msgid "Replace an existing instance"
msgstr "Thay thế một hiện thể đã có"

#: main.cpp:89
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "Truy vấn để chạy, chỉ dùng nếu không có -c"

#: qml/RunCommand.qml:93
#, kde-format
msgid "Configure"
msgstr "Cấu hình"

#: qml/RunCommand.qml:94
#, kde-format
msgid "Configure KRunner Behavior"
msgstr "Cấu hình ứng xử của KRunner"

#: qml/RunCommand.qml:97
#, kde-format
msgid "Configure KRunner…"
msgstr "Cấu hình KRunner…"

#: qml/RunCommand.qml:109
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr "Tìm kiếm '%1'…"

#: qml/RunCommand.qml:110
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr "Tìm kiếm…"

#: qml/RunCommand.qml:281 qml/RunCommand.qml:282 qml/RunCommand.qml:284
#, kde-format
msgid "Show Usage Help"
msgstr "Hiện trợ giúp sử dụng"

#: qml/RunCommand.qml:292
#, kde-format
msgid "Pin"
msgstr "Ghim"

#: qml/RunCommand.qml:293
#, kde-format
msgid "Pin Search"
msgstr "Ghim phần tìm kiếm"

#: qml/RunCommand.qml:295
#, kde-format
msgid "Keep Open"
msgstr "Giữ cho mở"

#: qml/RunCommand.qml:367 qml/RunCommand.qml:372
#, kde-format
msgid "Recent Queries"
msgstr "Các truy vấn gần đây"

#: qml/RunCommand.qml:370
#, kde-format
msgid "Remove"
msgstr "Xoá"

#~ msgid "krunner"
#~ msgstr "krunner"
