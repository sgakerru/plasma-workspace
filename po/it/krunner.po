# translation of krunner.po to Italian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Federico Zenith <federico.zenith@member.fsf.org>, 2007, 2008, 2009, 2010.
# Federico Zenith <federico.zenith@member.fsf.org>, 2008, 2012, 2014, 2015.
# Luigi Toscano <luigi.toscano@tiscali.it>, 2016, 2017, 2019, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: krunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-27 02:17+0000\n"
"PO-Revision-Date: 2022-09-20 00:21+0200\n"
"Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Federico Zenith"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "federico.zenith@member.fsf.org"

#: main.cpp:76 view.cpp:47
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:76
#, kde-format
msgid "Run Command interface"
msgstr "Interfaccia al comando di esecuzione"

#: main.cpp:82
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Usa i contenuti degli appunti come interrogazione per KRunner"

#: main.cpp:83
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "Esegui KRunner sullo sfondo, non mostrarlo."

#: main.cpp:84
#, kde-format
msgid "Replace an existing instance"
msgstr "Sostituisci un'istanza esistente"

#: main.cpp:89
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "L'interrogazione da eseguire, usata solo se -c non è presente"

#: qml/RunCommand.qml:93
#, kde-format
msgid "Configure"
msgstr "Configura"

#: qml/RunCommand.qml:94
#, kde-format
msgid "Configure KRunner Behavior"
msgstr "Configure il comportamento di KRunner"

#: qml/RunCommand.qml:97
#, kde-format
msgid "Configure KRunner…"
msgstr "Configure KRunner…"

#: qml/RunCommand.qml:109
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr "Cerca «%1»…"

#: qml/RunCommand.qml:110
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr "Cerca…"

#: qml/RunCommand.qml:281 qml/RunCommand.qml:282 qml/RunCommand.qml:284
#, kde-format
msgid "Show Usage Help"
msgstr "Mostra aiuto sull'uso"

#: qml/RunCommand.qml:292
#, kde-format
msgid "Pin"
msgstr "Fissa"

#: qml/RunCommand.qml:293
#, kde-format
msgid "Pin Search"
msgstr "Fissa ricerca"

#: qml/RunCommand.qml:295
#, kde-format
msgid "Keep Open"
msgstr "Mantieni aperto"

#: qml/RunCommand.qml:367 qml/RunCommand.qml:372
#, kde-format
msgid "Recent Queries"
msgstr "Interrogazioni recenti"

#: qml/RunCommand.qml:370
#, kde-format
msgid "Remove"
msgstr "Rimuovi"

#~ msgid "krunner"
#~ msgstr "krunner"

#~ msgid "Show KRunner"
#~ msgstr "Mostra KRunner"

#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "KRunner"
#~ msgstr "KRunner"

#~ msgid "Run Command on clipboard contents"
#~ msgstr "Esegui comando sui contenuti degli appunti"

#~ msgid "Run Command"
#~ msgstr "Esegui comando"

#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "Run Command"
#~ msgstr "Esegui comando"
